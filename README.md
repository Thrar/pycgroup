# PyCGroup -- Python interface to cgroups v1

**DEPERECATED:** cgroups v1 are deprecated.

This is a Python module to interact with cgroups v1 via sysfs. At the lower level, this module reads and write files under "/sys/fs/cgroups".

It is based on "drivers" for cgroup controllers (`cpu`, `cpuset`, `memory`...) written in the `controllers` modules, that expose the files of the cgroups controllers.

## Installation

This is a Python package that you can install using `pip` or even `pipenv`. It is not going to be published on PyPI
though, so you need to clone it and then install it from the local clone:

```shell script
git clone https://gitlab.com/Thrar/pycgroup
# cd into your project, set up with pipenv and containing a Pipfile
cd my_project
# install with pipenv
pipenv install ../PyCGroup
# alternatively, if you want pipenv to only "link" to PyCGroup's repo so modifications are naturally taken into account:
# pipenv install --editable ../PyCGroup
```

## Usage example

```python
from pycgroup import CGroup
from pycgroup.controllers import cpuset as cgcpuset

cg = CGroup(cgcpuset, 'test_cgroup')
cg.cpus = [0,1,2,3]
cg.mems = [0,1]

print(cg.cpus)

cg.add_pid(15000)
```
