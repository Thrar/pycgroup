"""PyCGroup --- interface to Linux cgroups

The main class is CGroup. An example usage is the following:

```
from pycgroup import CGroup
from pycgroup.controllers import cpuset as cgcpuset

cg = CGroup(cgcpuset, 'test_cgroup')
cg.cpus = [0,1,2,3]
cg.mems = [0,1]

print(cg.cpus)

cg.add_pid(15000)
```
"""

from .cgroup import CGroup, CGroupError
