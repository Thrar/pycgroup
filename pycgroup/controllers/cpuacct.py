"""cpuacct controller --- controller for cpuacct cgroups.

This is the cgroup controller for cpuacct cgroups, which are actually the same
as the cpu cgroups. So this controller corresponds to the "cpu,cpuacct"
hierarchy, whereas the cpu controller only corresponds to the "cpu" hierarchy.
"""

import sys

from ..cgroup import CGroupRoot
from . import cpu as cgcpu
from .abstract_controller import AbstractController


def for_cgroup(cgroup):
    """Return a CpuAcctCGroupController instantiated for the cgroup.

    This is called when instantiating a cgroup."""
    return CpuAcctCGroupController(cgroup)


class CpuAcctCGroupController(AbstractController):
    """cpuacct cgroup controller --- cpu,cpuacct controller for a cgroup."""
    ctrl_name = 'cpuacct'
    ctrl_dir = 'cpu,cpuacct'

    def __init__(self, cgroup, *args, **kwargs):
        super().__init__(cgroup, *args, **kwargs)
        # cpu controller to access CPU bandwidth controls
        self._cpuctrl = cgcpu.for_cgroup(cgroup)

    def get_stat(self):
        """Return the CPU stats as a dictionary."""
        with open(self._get_attr_path('stat'), 'r') as attrfile:
            return dict(
                (line.split()[0], int(line.split()[1])) for line in attrfile)

    def get_usage(self):
        with open(self._get_attr_path('usage'), 'r') as attrfile:
            return int(next(attrfile))

    def __getattr__(self, attr):
        try:
            # keep the magic attribute access from AbstractController
            return super().__getattr__(attr)
        except AttributeError:
            # if the magic access finds nothing, delegate to the cpu controller
            return getattr(self._cpuctrl, attr)

    def __setattr__(self, attr, val):
        try:
            super().__setattr__(attr, val)
        except AttributeError:
            setattr(self._cpuctrl, attr, val)


ROOT_CGROUP = CGroupRoot(sys.modules[__name__])
