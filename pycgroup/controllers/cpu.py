"""cpu controller --- controller for cpu cgroups.

This cgroup controller only controls the cpu cgroups. The latter are the same
as the cpuacct cgroups, meaning that you can have naming conflicts between the
two. If you only need CPU bandwidth control, use the cpu controller. If you
need the CPU accounting features, use the cpuacct controller.
"""

import sys

from ..cgroup import CGroupRoot
from .abstract_controller import AbstractController


def for_cgroup(cgroup):
    """Return a CpuCGroupController instantiated for the cgroup.

    This is called when instantiating a cgroup."""
    return CpuCGroupController(cgroup)


class CpuCGroupController(AbstractController):
    """cpu cgroup controller --- cpu controller for a cgroup."""
    ctrl_name = 'cpu'
    ctrl_dir = 'cpu'

    def get_cfs_quota_us(self):
        with open(self._get_attr_path('cfs_quota_us'), 'r') as attrfile:
            return int(next(attrfile))

    def set_cfs_quota_us(self, quota):
        with open(self._get_attr_path('cfs_quota_us'), 'w') as attrfile:
            attrfile.write(str(quota) + '\n')

    def get_cfs_period_us(self):
        with open(self._get_attr_path('cfs_period_us'), 'r') as attrfile:
            return int(next(attrfile))

    def set_cfs_period_us(self, period):
        with open(self._get_attr_path('cfs_period_us'), 'w') as attrfile:
            attrfile.write(str(period) + '\n')

    def get_shares(self):
        with open(self._get_attr_path('shares'), 'r') as attrfile:
            return int(next(attrfile))

    def set_shares(self, shares):
        with open(self._get_attr_path('shares'), 'w') as attrfile:
            attrfile.write(str(shares) + '\n')


ROOT_CGROUP = CGroupRoot(sys.modules[__name__])
