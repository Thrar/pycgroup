"""memory controller --- controller for memory cgroups.

"""

import sys

from ..cgroup import CGroupRoot
from .abstract_controller import AbstractController


def for_cgroup(cgroup):
    """Return a CpusetCGroupController instantiated for the cgroup.

    This is called when instantiating a cgroup."""
    return MemoryCGroupController(cgroup)


class MemoryCGroupController(AbstractController):
    """memory cgroup controller --- memory controller for a cgroup."""
    ctrl_name = 'memory'
    ctrl_dir = 'memory'

    def get_stat(self):
        """Return the memory stats as a dictionary."""
        with open(self._get_attr_path('stat'), 'r') as attrfile:
            return dict(
                (line.split()[0], int(line.split()[1])) for line in attrfile)

    def get_usage_in_bytes(self):
        with open(self._get_attr_path('usage_in_bytes'), 'r') as attrfile:
            return int(next(attrfile))

    def get_limit_in_bytes(self):
        with open(self._get_attr_path('limit_in_bytes'), 'r') as attrfile:
            return int(next(attrfile))

    def set_limit_in_bytes(self, limit):
        with open(self._get_attr_path('limit_in_bytes'), 'w') as attrfile:
            attrfile.write(str(limit) + '\n')

    def get_memsw_limit_in_bytes(self):
        with open(self._get_attr_path('memsw.limit_in_bytes'), 'r') as attrfile:
            return int(next(attrfile))

    def set_memsw_limit_in_bytes(self, limit):
        with open(self._get_attr_path('memsw.limit_in_bytes'), 'w') as attrfile:
            attrfile.write(str(limit) + '\n')


ROOT_CGROUP = CGroupRoot(sys.modules[__name__])
