"""Controllers --- implemented cgroup controllers.

The modules in this package are passed as the first parameter of the CGroup
constructor to define the type of the created cgroup.

The modules define a method for_cgroup to be used as a factory to get a cgroup
controller of the corresponding type, for a given cgroup.

The cgroup controllers that are returned, are classes that expose getters and
setters to the cgroup attributes. These instances also allow for attribute access
with classic object attribute access in Python (i.e. with the dot syntax).
"""

__all__ = ['cpuset', 'cpu', 'cpuacct', 'memory']
