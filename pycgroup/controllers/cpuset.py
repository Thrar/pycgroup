"""cpuset controller --- controller for cpuset cgroups.

This controller requires that the attributes "cpus" and "mems" be set (they are
initially empty). Otherwise, adding a PID will fail on an errno 28 "no space left on
device", which raises a corresponding exception in Python. This exception however
seems not to be catchable.
"""

import sys

from ..cgroup import CGroupRoot
from ..controllers_utils import range2list
from .abstract_controller import AbstractController


def for_cgroup(cgroup):
    """Return a CpusetCGroupController instantiated for the cgroup.

    This is called when instantiating a cgroup."""
    return CpusetCGroupController(cgroup)


class CpusetCGroupController(AbstractController):
    """cpuset cgroup controller --- cpuset controller for a cgroup."""
    ctrl_name = 'cpuset'
    ctrl_dir = 'cpuset'

    def get_cpus(self):
        """Return the CPUs given to the cgroup.

        Return a list of CPU IDs, starting at 0.
        """
        with open(self._get_attr_path('cpus'), 'r') as attrfile:
            return range2list(attrfile.read().rstrip())

    def set_cpus(self, cpus):
        """Set the CPUs to give to the cgroup.

         * cpus: list of CPU IDs
        """
        with open(self._get_attr_path('cpus'), 'w') as attrfile:
            # EOL is important to force writing an empty line (in case cpus is the
            # empty list), otherwise it does nothing to the cgroup file (Python
            # doesn't write anything, or the cgroup sysfs module does not react).
            attrfile.write(','.join(str(cpu) for cpu in cpus) + '\n')

    def get_mems(self):
        """Return the memory nodes given to the cgroup.

        Return a list of memory nodes IDs, starting at 0.
        """
        with open(self._get_attr_path('mems'), 'r') as attrfile:
            return range2list(attrfile.read().rstrip())

    def set_mems(self, mems):
        """Set the memory nodes to give to the cgroup.

         * mems: list of memory nodes IDs
        """
        with open(self._get_attr_path('mems'), 'w') as attrfile:
            attrfile.write(','.join(str(mem) for mem in mems) + '\n')


ROOT_CGROUP = CGroupRoot(sys.modules[__name__])
