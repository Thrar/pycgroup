"""Abstract controller --- abstract base class for cgroup controllers

Controllers must implement a class that extends this abstract class. Concrete
implementation must override the ctrl_name class member.

Beside providing a child class of this abstract class, controller modules must
provide two module-level attributes:

 * DIRECTORY: the name of the directory of the controller under the sysfs hierarchy
 * ROOT_CGROUP: the root cgroup of the controller (a CGroupRoot instance)

Finally, they must also provide a method for_cgroup that returns an instance of the
cgroup controller set up for a given cgroup. This can be as simple as just
instantiating the cgroup controller with the given cgroup as parameter.
"""

from abc import ABCMeta
from errno import EINVAL
from os.path import join as pjoin

from ..cgroup import CGroupError


class AbstractController(metaclass=ABCMeta):
    """AbstractController --- abstract base class for cgroup controllers

    It defines a helper method to get the path to the control file in the sysfs for
    a given attribute. It also sets the attribute access handling to redirect such
    access to getter and setters calls that actually get and set the cgroup
    attributes.

    Attribute accesses may raise CGroupError on cgroup related error, of ValueError
    when trying to set an attribute to an invalid value. AttributeError is raise
    when trying to access an unknown attribute.
    """
    ctrl_name = 'abstract'
    ctrl_dir = 'abstract'

    def __init__(self, cgroup):
        self._cgroup = cgroup

    def _get_attr_path(self, attr):
        """Return the path to the file associated with the given attribute."""
        return pjoin(self._cgroup.get_path(), type(self).ctrl_name + '.' + attr)

    def __getattr__(self, attr):
        """Redirect attribute access to the dedicated getter."""
        getter = 'get_' + attr
        if not getter in dir(self):
            raise AttributeError(attr)

        return getattr(self, 'get_' + attr)()

    def __setattr__(self, attr, val):
        """Redirect attribute access to the dedicated setter."""
        # avoid infinite recursion in __init__ when initializing attributes
        if attr.startswith('_'):
            super().__setattr__(attr, val)
            return

        try:
            getattr(self, 'set_' + attr)(val)
        except OSError as err:
            if err.args[0] == EINVAL:
                raise ValueError(attr, val)
            else:
                raise CGroupError from err

    def __str__(self):
        return f'{type(self).ctrl_name} (for {self._cgroup})'


ROOT_CGROUP = None
