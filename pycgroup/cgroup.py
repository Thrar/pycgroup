"""CGroup --- cgroup manager.

The class CGroup represents a cgroup of whatever controller type that was specified
at creation.

Its usage boils down to initializing it by specifying the controller (i.e. the
cgroup type), giving it a name, and optionally placing it under a parent cgroup; and
then setting its attributes and finally adding PIDs.
"""
from os import mkdir, rmdir
from os.path import join as pjoin

CGROUP_BASEPATH = '/sys/fs/cgroup'


class CGroupError(Exception):
    """Raise when an error occurs on a cgroup operation."""
    pass


class CGroup:
    """CGroup --- Representation of a cgroup.

    It holds the PIDs that are in the cgroup, and gives access to the cgroup's
    attributes through the controller.

    Accessing cgroup attributes is done via accessing the object's attributes. For
    instance, to get the CPUs of a cpuset cgroup, which is the attribute named
    "cpus", you can type `print(cgroup.cpus)`. To set them, you can type
    `cgroup.cpus = [0,1,2,3]`.

    Accessing an unknown (or unimplemented) attribute raises AttributeError. Trying
    to set an attribute to an invalid value raises ValueError.

    The underlying cgroup is created upon the creation of the object.
    """

    def __init__(self, ctrl, name, parent=None, reuse=False):
        """Create a new CGroup.

         * ctrl: cgroup controller (i.e. the type of the cgroup, like cpuset or
         memory), it is a module from the "controllers" subpackage
         * name: name of the cgroup, it must be a valid directory name
         * parent: parent of the cgroup (optional, default is the root cgroup of the
         given type)
         * reuse: whether to reuse an existing cgroup with the same name (default is
         not to reuse, and thus to fail if a cgroup with the given name already
         exists)

        Raise CGroupError is a cgroup of this controller type already exists with
        the same name, and reuse is False (the default).
        """
        self.ctrl = ctrl.for_cgroup(self)
        self.name = name

        if parent is None:
            self.parent = ctrl.ROOT_CGROUP
        else:
            self.parent = parent

        try:
            mkdir(self.get_path())
        except OSError as err:
            if err.args[0] != 17 or not reuse:
                raise CGroupError(f'failed creating cgroup') from err

    def get_path(self):
        """Return the path to the cgroup in sysfs."""
        return pjoin(self.parent.get_path(), self.name)

    def add_pid(self, pid):
        """Add the PID to the cgroup.

        Make sure the cgroup is ready and set to accept new PIDs. For instance,
        cpuset cgroups must have their attributes cpus and mems set before adding a
        PID. Otherwise, a (seemingly) uncatchable error is raised.

        Raise CGroupError on failure (but it may also raise uncatchable OSError).
        """
        with open(self._get_attr_path('procs'), 'w') as procsfile:
            try:
                procsfile.write(str(pid))
            except OSError as err:
                # OSError is not caught on errno 28 "no space left" (it happens when
                # adding a PID to a cpuset cgroup that has no memory node or no CPU
                # node). I don't know why, I suspect some kind of special case for
                # the Python interpreter.
                raise CGroupError(f'failed adding PID {pid}') from err

    def get_pids(self):
        """Return the list of PIDs that were added to the cgroup."""
        with open(self._get_attr_path('procs'), 'r') as procsfile:
            return [int(line) for line in procsfile]

    def destroy(self):
        """Destroy the cgroup.

        This operation adds the PIDs that remain in the cgroup to the cgroup's
        parent, before removing the cgroup's directory in sysfs.

        Raise CGroupError on failure.
        """
        for pid in self.get_pids():
            self.parent.add_pid(pid)
        try:
            rmdir(self.get_path())
        except OSError as err:
            raise CGroupError(f'failed destroying cgroup') from err

    def _get_attr_path(self, attr):
        """Return the name of the file associated with the given attribute."""
        return pjoin(self.get_path(), 'cgroup.' + attr)

    def __getattr__(self, attr):
        """Redirect attribute access to the controller."""
        try:
            return getattr(self.ctrl, attr)
        except AttributeError:
            raise AttributeError(f'controller {self.ctrl} does not know attribute '
                                 f'"{attr}"') from None

    def __setattr__(self, attr, val):
        """Redirect attribute access to the controller."""
        if attr in ['ctrl', 'name', 'parent']:
            super().__setattr__(attr, val)
            return

        try:
            setattr(self.ctrl, attr, val)
        except AttributeError:
            raise AttributeError(f'controller {self.ctrl} does not know attribute '
                                 f'"{attr}"') from None
        except ValueError:
            raise ValueError(
                f'bad value "{val}" for attribute "{attr}" of controller '
                f'{self.ctrl}') from None

    def __str__(self):
        return self.name


class CGroupRoot(CGroup):
    """Special case of CGroup to represent root cgroups.

    We "hack" the CGroup base class to bypass the cgroup creation and setting its
    parent. We also override the get_path method for this special case.

    Each controller instantiates its own root cgroup.
    """

    def __init__(self, ctrl):
        self.ctrl = ctrl.for_cgroup(self)
        self.name = ''
        self.parent = None

    def get_path(self):
        return pjoin(CGROUP_BASEPATH, self.ctrl.ctrl_dir)
