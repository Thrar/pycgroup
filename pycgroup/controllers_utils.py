"""Common utility code for controllers."""

from enum import Enum, auto


class RangeParserStates(Enum):
    """States for the range parser state machine."""
    Subrange = auto()
    Identifier = auto()


def range2list(rng):
    """Parse a range of IDs to a list.

    A range of IDs is something like "0,2,4-7,12". This is encountered in the cpuset
    controller for instance, as the value read from the cpus of the mems attributes.
    """
    if not rng:
        return []

    rnglist = []

    state = RangeParserStates.Subrange
    subrange_start = None

    # we add a comma at the end to trigger the appending of the current subrange
    for char in rng + ',':
        if state is RangeParserStates.Subrange:
            if not char.isdigit():
                raise ValueError(f'expected digit, got "{char}"')
            cur_id = char

            state = RangeParserStates.Identifier
        elif state is RangeParserStates.Identifier:
            if char.isdigit():
                cur_id += char
            elif char == ',':
                cur_id = int(cur_id)
                if subrange_start is not None:
                    rnglist.extend(range(subrange_start, cur_id + 1))
                    subrange_start = None
                else:
                    rnglist.append(cur_id)

                state = RangeParserStates.Subrange
            elif char == '-':
                if subrange_start is not None:
                    raise ValueError(f'expected digit or ",", got "{char}"')
                subrange_start = int(cur_id)
                cur_id = ''

                state = RangeParserStates.Subrange

    return list(set(rnglist))


# Note: no need for list2range, it is enough to ','.join() the discrete values. It
# does no yield the "optimal" range expression (it does not generate contiguous
# ranges with the hyphen), but the underlying cgroup controllers accept that very
# well and will actually do the list2range operation themselves.
